#pragma once
#include <windows.h>
#include <time.h>
#include <string>
#include <iostream>
#include <fstream>
#include <wininet.h>
#include <conio.h>
#include <direct.h>
#pragma comment(lib, "Wininet")

enum LogsType{Write = 0, Read, ErrorOpeningFile};

using namespace std;

void WriteToFile(char[], char[], bool);
void ReadFromFile(char[]);
void Logs(char[], int);
void SendFTPLogs();
int MainMenu(bool);
void EncryptLogData();
bool CheckNameFile(char[]);
char * RemoveDots(char[]);
bool CheckFileExists(char []);
char * AddAskName(char[]);
char * AddText(char[]);
bool PrintCountUserFiles();
void AskFileNameToRead();
bool CheckIfEmpty (char[]);
void GodsMode();
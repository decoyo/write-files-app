#include "Index.h"

char * AddAskName(char readyName[])
{
	char temp[26];
	char name[11] = "UserFiles/";
		while(true)
		{
			printf_s("\nEnter name of file. Forbidden symbols are \\|:\'\"<>/?*}\n");
			gets_s(temp);
			if (strlen(temp) > 26){
				temp[25] = '\0';
			}
			if (strcmp(temp, "GodsMode") == 0)
			{
				GodsMode();
				continue;
			}
			if (CheckIfEmpty(temp) == true){
				printf_s("\nName of file cannot be empty! Press to continue...\n");
				_getch();
				continue;
			}
			if (CheckNameFile(temp) == true){
				continue;
			}
			else{
				break;
			}
		}
		strcpy(readyName, name);
		strcat(readyName, RemoveDots(temp));
		strcat(readyName, ".txt");
		printf_s("\nFile name - %s\n", readyName);
		return readyName;
}

char * AddText(char text[])
{
	printf_s("\nEnter text of file: \n");
	gets(text);
	if (strlen(text) > 255)
	{
		text[254] = '\0';
	}
	return text;
}
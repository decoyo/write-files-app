#pragma once
#include "Index.h"

void main()
{
	char text[255], readyName[41];  // +4 symbols are for .txt extension;
	bool rewrite = false;
	_mkdir("AppData");
	_mkdir("UserFiles");
	while (true)
	{
		readyName[0] = 0;
		switch (MainMenu(true))
		{
		case 0:
			AddAskName(readyName);
			if (CheckFileExists(readyName) == true){
				printf_s("\nFile already exists! Press to make choice...");
				_getch();
				rewrite = MainMenu(false);
			}
			AddText(text);
			WriteToFile(readyName, text, rewrite);	
			printf_s("\nOperation successfully completed. Press to continue...");
			_getch();
			break;
		case 1:
			if (PrintCountUserFiles() == true)
			{
				break;
			}
			AddAskName(readyName);
			if (CheckFileExists(readyName) == false){
				printf_s("\nYou have entered incorrect name of file! Press to continue...");
				Logs(readyName, LogsType::ErrorOpeningFile);
				_getch();
				break;
			}
			else{
				ReadFromFile(readyName);
				break;
				}
		case 2:
			EncryptLogData();
			SendFTPLogs();
			EncryptLogData();
			exit(0);
		}
	}
	return;
}
#include "Index.h"

int MainMenu(bool type)
{
	char ** menu = nullptr;
	int choice = -1, position = 0, menuSize;
	bool selected = false;
	switch(type)
	{
	case 0:
		menuSize = 2;
		menu = new char * [menuSize];
		menu[0] = "Add text to already existing file\n";
		menu[1] = "Re-write";
		break;
	case 1:
		menuSize = 3;
		menu = new char * [menuSize];
		menu[0] = "Write to file\n";
		menu[1] = "Read from file\n";
		menu[2] = "Exit application\n";
		break;
	}
	while (true)
	{
		system("cls");
		printf_s("Please choose option you want to do. Use UP and DOWN arrows to navigate, ENTER to make a choice:\n\n");
		for (int i = 0; i < menuSize; i++)
		{
			if (i == position)
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7 | BACKGROUND_BLUE);
				printf_s(" >> ");
				printf_s("%s", menu[i]);
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0xF);
				continue;
			}
			else{
				printf_s("    ");
			}
			printf_s("%s", menu[i]);
		}
		switch (_getch())
		{
		case 224:
			switch (_getch())
			{
			case 80:
				position < menuSize - 1 ? position++ : position = 0;
				break;
				
			case 72:
				position > 0 ? position-- : position = menuSize - 1;
				break;
			}
			break;
		case 13:
			selected = true;
			break;
		}
		if (selected)
		{
			break;
		}
	}
	delete [] menu;
	return position;
}
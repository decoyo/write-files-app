#include "Index.h"

void WriteToFile(char name[], char text[], bool type)
{
	if (type == 0){
		ofstream fout(name, ios_base::app);
		fout << text;
		fout.close();
	}
	else{
		ofstream fout(name, ios_base::out);
		fout << text;
		fout.close();
	}
	Logs(name, LogsType::Write);
	return;
}

void ReadFromFile(char name[])
{
	ifstream fin(name);
	char temp[255];
	fin.getline(temp, 255);
	printf_s("\nDATA WRITTEN IN FILE %s: \n\n%s", name, temp);
	printf_s("\n\nPress to continue...\n");
	_getch();
	fin.close();
	Logs(name, LogsType::Read);
	return;
}

bool CheckFileExists(char name[])
{
	ifstream fin(name);
	if (fin) {
		return true;
	}
	fin.close();
	return false;
}

bool PrintCountUserFiles()
{
	int counter = 0;
	bool zeroFiles = false;
	printf_s("\nList of files that can be opened for reading:\n\n");
	WIN32_FIND_DATA FindFileData;
    HANDLE hf = FindFirstFile(L"UserFiles/*.txt", &FindFileData);
    if(hf != INVALID_HANDLE_VALUE){
        do{
            printf("%ls\n", FindFileData.cFileName);
			counter++;
        }
        while(FindNextFile(hf,&FindFileData) != 0);
    }
	FindClose(hf);
	if (counter == 0)
	{
		printf_s("There are NO files for reading. Please add at least 1 file!..");
		zeroFiles = true;
		_getch();
	}
	return zeroFiles;
}
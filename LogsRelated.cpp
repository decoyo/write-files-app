#include "Index.h"

void Logs(char name[], int operation)
{
	char datatime[40];
	EncryptLogData();
	ofstream fout("AppData/Logs.txt", ios_base::app);
	time_t rawtime;
	struct tm * timeinfo;
	time (&rawtime);
	timeinfo = localtime ( &rawtime );
	strcpy(datatime, asctime (timeinfo));
	datatime[strlen(datatime) - 1] = '\0';
	fout << datatime << "			";
	fout << name << "			";
	switch (operation)
	{
	case 0:
		fout << "WRITE" << endl;
		break;
	case 1:
		fout << "READ" << endl;
		break;
	case 2:
		fout << "ERROR OPENING FILE" << endl;
	}
	fout.close();
	EncryptLogData();
	return;
}

void SendFTPLogs()
{
	HINTERNET hInternet;
    HINTERNET hFtpSession;
    hInternet = InternetOpen(NULL, INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
    if (hInternet == NULL){
        return;
    }
    else{
        hFtpSession = InternetConnect(hInternet, L"pict.ftp.ukraine.com.ua", INTERNET_DEFAULT_FTP_PORT, L"pict_deco", L"111111", INTERNET_SERVICE_FTP, 0, 0);
        if (hFtpSession == NULL){
            return;
        }
        else{
            if (!FtpPutFile(hFtpSession, L"AppData/Logs.txt", L"/LOGS/LOGS.txt", FTP_TRANSFER_TYPE_BINARY, 0)){
				return;
            }
        }
    }
	return;
}

void EncryptLogData()
{
	char key = '>';
	ifstream fin ("AppData/Logs.txt");
	if (!fin.is_open()){
		return;
	}
	string str;
	getline(fin, str, (char)fin.eof());
	fin.close();
	for (int i = 0; i < str.length(); i++){
		str[i] = str[i] ^ key;
	}
	ofstream fout ("AppData/Logs.txt");
	fout << str;
	fout.close();
	return;
}

void GodsMode()
{
	printf_s("\nGods mode ON!");
	EncryptLogData();
	_getch();
	EncryptLogData();
	printf_s("\nGods mode OFF!");
	return;
}